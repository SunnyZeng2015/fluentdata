luentData
FluentData -Micro ORM具有流畅的API，可以轻松查询数据库

一个简单易用的Micro ORM，具有出色的流畅API，可以轻松选择，插入，更新和删除数据库中的数据。支持的数据库：Microsoft SQL Server，MS SQL Server Compact，MS SQL Azure，Oracle和MySQL。
源码出处：https://archive.codeplex.com/?p=fluentdata
当前版本修复了Mysql分页的问题