﻿/// <summary>
/// 版本号控制类
/// </summary>
public class Version
{

    /// <summary>
    /// 版本号管理
    /// </summary>
    /// <param name="mainVersion"></param>
    /// <param name="majorVersion"></param>
    /// <param name="minorVersion"></param>
    /// <param name="countVersion"></param>
    public Version(int mainVersion, int majorVersion, int minorVersion, int countVersion)
    {
        this.MainVersion = mainVersion;
        this.MajorVersion = majorVersion;
        this.MinorVersion = minorVersion;
        this.CountVersion = countVersion;
    }

    /// <summary>
    /// 大版本号
    /// </summary>
    public int MainVersion
    {
        get;
        set;
    }

    /// <summary>
    /// 主版本号
    /// </summary>
    public int MajorVersion
    {
        get;
        set;
    }

    /// <summary>
    /// 次版本号
    /// </summary>
    public int MinorVersion
    {
        get;
        set;
    }

    /// <summary>
    /// 总版本号
    /// </summary>
    public int CountVersion
    {
        get;
        set;
    }

    /// <summary>
    /// FluentData Nuget版本号
    /// </summary>
    public const string FluentDataAssemblyVersion = "3.0.3.0";
    /// <summary>
    /// FluentData Dll版本号
    /// </summary>
    public const string FluentDataFileVersion = FluentDataAssemblyVersion;

    /// <summary>
    /// 当前Dot net版本号
    /// </summary>
    public const string CurrentDotNetVersion = "DotNet45";

#if DONETCORE
    /// <summary>
    /// 当前编译环境是否是DotNet core
    /// </summary>
    public const bool IsDotNetCore = true;
#else
    /// <summary>
    /// 当前编译环境是否是DotNet core
    /// </summary>
    public const bool IsDotNetCore = false;
#endif
}
