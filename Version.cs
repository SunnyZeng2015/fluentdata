﻿public class Version
{
    public Version()
    {

    }

    public Version(int mainVersion, int majorVersion, int minorVersion, int countVersion)
    {
        this.MainVersion = mainVersion;
        this.MajorVersion = majorVersion;
        this.MinorVersion = minorVersion;
        this.CountVersion = countVersion;
    }

    /// <summary>
    /// 大版本号
    /// </summary>
    public int MainVersion
    {
        get;
        set;
    }

    /// <summary>
    /// 主版本号
    /// </summary>
    public int MajorVersion
    {
        get;
        set;
    }

    /// <summary>
    /// 次版本号
    /// </summary>
    public int MinorVersion
    {
        get;
        set;
    }

    /// <summary>
    /// 总版本号
    /// </summary>
    public int CountVersion
    {
        get;
        set;
    }

    // 内部依赖
    // Crm.Utility
    // Crm.WebAPI.NewCore
    // 外部依赖
    // Zhan.Platform.Interface 1.0.1.4
    // Zhan.Platform.Log 1.0.1.4
    // Zhan.WebAPI.Core 1.0.0.2
    public const string ZhanWebAPIIocAssemblyVersion = "1.0.0.7";
    public const string ZhanWebAPIIocFileVersion = ZhanWebAPIIocAssemblyVersion;

    public const string ZhanWebAPICoreAssemblyVersion = "1.0.0.3";
    public const string ZhanWebAPICoreFileVersion = ZhanWebAPICoreAssemblyVersion;

    /// <summary>
    /// 当前Dot net版本号
    /// </summary>
    public const string CurrentDotNetVersion = "DotNet461";

#if DONETCORE
    /// <summary>
    /// 当前编译环境是否是DotNet core
    /// </summary>
    public const bool IsDotNetCore = true;
#else
    /// <summary>
    /// 当前编译环境是否是DotNet core
    /// </summary>
    public const bool IsDotNetCore = false;
#endif
}
