import clr
import System
import sys

def Reference(refs):
    imps = [('from %s import *' % i) for i in refs ]
    for i in refs:
        try:
            clr.AddReference(i)
        except Exception:
            pass   
    for i in imps:      
        try:
            exec(compile(i, '', 'exec'))
        except Exception:
            pass 


