class imp:
    def importModule():
        #导入python库引用
        import sys
        sys.path.append(r"Python\IronPythonLib2.7")
        #导入dot net引用
        import clr
        refs = [
            "System", "System.Drawing", "System.Collections.Generic", "System.IO", "System.Text", "System.Web"
        ]
        imps = [('from %s import *' % i) for i in refs ]
        for i in refs:
            try:
                clr.AddReference(i)
            except Exception:
                pass   
        for i in imps:              
            try:
                exec(compile(i, '', 'exec'))
            except Exception:
                pass  
        #添加对第三方库的引用
        clr.AddReferenceToFile("Sandz.dll")
        #from Sandz.Utility.StringUtil import *
        #from Sandz.Utility.MachUtil import *
