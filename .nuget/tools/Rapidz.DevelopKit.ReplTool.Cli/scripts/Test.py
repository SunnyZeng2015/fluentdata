#coding=utf-8
import clr, System, sys, nt

now_path = System.AppDomain.CurrentDomain.BaseDirectory;
sys.path.append(r'Python\IronPythonLib2.7')
refs = [
    'System', 'System.Diagnostics', 'System.Text', 'System.DateTime', 'System.Drawing', 'System.Collections.Generic', 'System.IO', 'System.Linq', 'System.Reflection', 'System.Reflection.Emit', 'System.Text', 'System.Net', 'System.Xml.Linq'
]
imps = [('from %s import *' % i) for i in refs ]
for i in refs:
    try:
        clr.AddReference(i)
    except Exception:
        pass   
for i in imps:      
    try:
        exec(compile(i, '', 'exec'))
    except Exception:
        pass 

clr.AddReferenceToFile('Rapidz.dll')
from Rapidz.Utility.ByteStreamMemoryUtil import *
from Rapidz.Utility.StringUtil import *
from Rapidz.Utility.HttpUtil import *
from Rapidz.Utility.FileDirectoryUtil import *

clr.AddReferenceToFile('Newtonsoft.Json.dll')
from Newtonsoft.Json import *
from Newtonsoft.Json.Linq import *


def generate_domain(year, month, day):
    domain = ""
  
    for i in range(16):
        year = ((year ^ 8 * year) >> 11) ^ ((year & 0xFFFFFFF0) << 17)
        month = ((month ^ 4 * month) >> 25) ^ 16 * (month & 0xFFFFFFF8)
        day = ((day ^ (day << 13)) >> 19) ^ ((day & 0xFFFFFFFE) << 12)
        domain += chr(((year ^ month ^ day) % 25) + 97)
  
    return domain;

this.ResultFormatLine('{0}-{1}-{2}', generate_domain(2000, 1, 12), this.Parameter['age'], this.Parameter['name']);
